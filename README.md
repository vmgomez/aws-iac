<p align="center">
  <img src="https://example.com/your-logo.png" alt="Your Repository Logo" width="200">
</p>



# 🚀 CloudFormation Templates Repository 🚀

[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Welcome to the CloudFormation Templates Repository! This repository contains a collection of AWS CloudFormation templates to help you deploy and manage your AWS resources effortlessly.



## 🔧 How to Use

1. First, clone this repository to your local machine.
git clone https://gitlab.com/vmgomez/aws-iac/


2. Browse the `cloudformation/` directory to find the CloudFormation templates organized by categories.

3. Deploy the template of your choice using the AWS Management Console or the AWS CLI.



## 📝 Contributing

We welcome contributions from the community! If you find any issues or have improvements to suggest, please feel free to open an issue or create a pull request.



## 📜 License

This project is licensed under the [MIT License](LICENSE).



## 📧 Contact

If you have any questions or need assistance, send me message, ty!


Happy cloud deploying! 🌥️💻